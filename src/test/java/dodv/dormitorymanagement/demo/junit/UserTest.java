package dodv.dormitorymanagement.demo.junit;

import dodv.dormitorymanagement.demo.auth.ApplicationUserService;
import dodv.dormitorymanagement.demo.dto.request.*;
import dodv.dormitorymanagement.demo.dto.response.*;
import dodv.dormitorymanagement.demo.entity.InsuranceFee;
import dodv.dormitorymanagement.demo.entity.User;
import dodv.dormitorymanagement.demo.entity.UserIncome;
import dodv.dormitorymanagement.demo.enumuration.EUserClass;
import dodv.dormitorymanagement.demo.repository.UserIncomeRepository;
import dodv.dormitorymanagement.demo.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import javax.crypto.SecretKey;
import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static dodv.dormitorymanagement.demo.enumuration.EUserClass.DOMESTIC_LABOR;
import static dodv.dormitorymanagement.demo.enumuration.EUserClass.FOREIGN_LABOR;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
// Số lượng: 11
public class UserTest {
    @Autowired
    private ApplicationUserService applicationUserService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserIncomeRepository userIncomeRepository;
    @Autowired
    private SecretKey secretKey;

    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private User userLogin;

    // Chức năng: Đăng ký
    @Test
    @Rollback
    @Transactional
    // Input: RegisterRequestDTO {username = "user1", password = "123456",
    // confirmPassword = "123456", insuranceCode = "MBH0456789"}
    // Expect output: Tồn tại User trong db
    public void registerValid() {
        try {
            RegisterRequestDTO dto = new RegisterRequestDTO("MBH0456789", "user1",
                    "123456", "123456");
            applicationUserService.register(dto);
            User user = userRepository.findByUsername("user1");
            assertNotNull(user);
            assertEquals(user.getInsuranceCode(), "MBH0456789");
        } catch (Exception e) {
            Assert.fail("Lỗi không lấy được danh sách đăng ký");
        }
    }

    @Test
    @Rollback
    @Transactional
    // Chức năng: Đăng ký - Khách hàng đã tồn tại insuranceCode
    // Input: RegisterRequestDTO {username = "user1", password = "123456"
    // confirmPassword = "123456", insuranceCode = "MBH0456789"}, đăng ký 2 lần
    // Expect output: Đăng ký thất bại => Exception
    public void registerUsernameInvalid() {
        try {
            RegisterRequestDTO dto = new RegisterRequestDTO("MBH0456789", "user1",
                    "123456", "123456");
            applicationUserService.register(dto);
            Throwable throwable = assertThrows(ResponseStatusException.class, () -> applicationUserService.register(dto));
            assertEquals(((ResponseStatusException) throwable).getReason(), "Đăng ký thất bại");
        } catch (Exception e) {
            Assert.fail("Lỗi không lấy được danh sách đăng ký");
        }
    }

    // Chức năng: Đăng nhập
    @Before
    @Test
    // Input: LoginRequestDTO {username = "admin", password = "admin"}
    // Expect output: Đăng nhập thành công (status = 200)
    public void loginValid() {
        LoginRequestDTO dto = new LoginRequestDTO("admin", "admin");
        ResponseEntity result = applicationUserService.login(dto);
        assertEquals(200, result.getStatusCodeValue());
        String token = ((JWTResponseDTO) (result.getBody())).getToken();
        setAuthenByToken(token);
        userLogin = userRepository.findByUsername(dto.getUsername());
    }

    @Test
    @Rollback
    @Transactional
    // Input: LoginRequestDTO {username = "admin", password = "12345"}
    // Expect output: Đăng nhập thất bạn (status = 417, body: ErrorDTO)
    public void loginInvalid() {
        LoginRequestDTO dto = new LoginRequestDTO("admin", "12345");
        ResponseEntity result = applicationUserService.login(dto);
        assertEquals(result.getStatusCodeValue(), 417);
        assertEquals(((ErrorDTO) result.getBody()).getError(), "Your username/password is not correct");
    }

    // Chức năng: Cập nhật thông tin user
    @Test
    @Rollback
    @Transactional
    // Input: DeclareInformationDTO {fullName = "full_name_1", phoneNumber = "0123456789",
    // email = "user@gmail.com", isEmployee = true, userClass = DOMESTIC_LABOR}
    // Expect output: Tồn tại User trong db
    public void updateProfileValid() {
        DeclareInformationDTO dto = new DeclareInformationDTO("full_name_1", "0123456789",
                "user@gmail.com", true, DOMESTIC_LABOR);
        applicationUserService.updateProfile(dto);
        User user = userRepository.findByUsername("admin");
        assertEquals(user.getFullName(), dto.getFullName());
        assertEquals(user.getEmail(), dto.getEmail());
        assertEquals(user.getIsEmployee(), dto.getIsEmployee());
    }

    // Chức năng: Khai báo thu nhập
    @Test
    @Rollback
    @Transactional
    // Input: Nhập thông tim income cho user đều đúng
    // Expect output: Tồn tại UserIncome vừa thêm trong db
    public void declareIncomesValid() {
        try {
            List<DeclareIncomeDTO> incomeDTOS = new ArrayList<>();
            incomeDTOS.add(new DeclareIncomeDTO(sdf.parse("01/01/2022"), sdf.parse("31/03/2022"), 20000000));
            incomeDTOS.add(new DeclareIncomeDTO(sdf.parse("01/03/2021"), sdf.parse("30/04/2021"), 30000000));
            DeclareIncomeRequestDTO dto = new DeclareIncomeRequestDTO(incomeDTOS);
            applicationUserService.declareIncome(dto);
            List<UserIncome> incomes = userIncomeRepository.getUserIncomeByUser(userLogin);
            assertNotNull(incomes);
            assertEquals(incomes.get(incomes.size() - 2).getIncome(), (Integer) 20000000);
            assertEquals(incomes.get(incomes.size() - 1).getIncome(), (Integer) 30000000);
        }  catch (Exception e) {
            Assert.fail("Lỗi không lưu được giá trị trong db");
        }
    }

    @Test
    @Rollback
    @Transactional
    // Input: Nhập sai thời gian một giá trị income (startTime > endTime)
    // Expect output: Bắt được lỗi bằng throwable
    public void declareIncomesInvalid() {
        try {
            List<DeclareIncomeDTO> incomeDTOS = new ArrayList<>();
            incomeDTOS.add(new DeclareIncomeDTO(sdf.parse("01/01/2022"), sdf.parse("31/03/2022"), 20000000));
            incomeDTOS.add(new DeclareIncomeDTO(sdf.parse("01/03/2022"), sdf.parse("28/02/2022"), 30000000));
            DeclareIncomeRequestDTO dto = new DeclareIncomeRequestDTO(incomeDTOS);
            Throwable throwable = assertThrows(ResponseStatusException.class, () -> applicationUserService.declareIncome(dto));
            assertEquals(((ResponseStatusException) throwable).getReason(), "Your income your declared is not valid");
        }  catch (Exception e) {
            Assert.fail("Lỗi không check được trường hợp ngoại lệ");
        }
    }

    @Test
    @Rollback
    @Transactional
    // Input: null
    // Expect output: Lấy được thông tin insurance fee tương ứng
    public void userFilter() {
        try {
            List<FilterDRO> filters = applicationUserService.getUserClassFilter();
            System.out.println(filters.size());
            assertNotNull(filters);
            assertEquals(filters.size(), 3);
            assertEquals(filters.get(0).getValue(), "DOMESTIC_LABOR");
            assertEquals(filters.get(1).getValue(), "FOREIGN_LABOR");
            assertEquals(filters.get(2).getValue(), "OFFICER");
        }  catch (Exception e) {
            Assert.fail("Lỗi không check được trường hợp ngoại lệ");
        }
    }

    @Test
    @Rollback
    @Transactional
    public void insuranceConfigSuccess() {
        try {
            List<InsuranceFee> list = applicationUserService.getInsuranceConfig();
            assertNotNull(list);
            assertEquals(list.size(), 18);
            assertEquals(list.get(0).getStartTime(), sdf.parse("01/01/2010"));
            assertEquals(list.get(0).getEndTime(), sdf.parse("31/12/2011"));
            assertEquals(list.get(0).getIsEmployee(), true);
            assertEquals(list.get(0).getUserClass(), DOMESTIC_LABOR);
            assertEquals(list.get(0).getValue(), 6.0, 0);
        } catch (Exception e) {
            Assert.fail("Lỗi không check được trường hợp ngoại lệ");
        }
    }

    @Test
    @Rollback
    @Transactional
    public void getProfile() {
        try {
            UserInfoDTO info = applicationUserService.getMyProfile();
            System.out.println(info);
            assertEquals("full_name_1", info.getFullName());
            assertEquals("admin", info.getUsername());
            assertEquals("0123456789", info.getPhoneNumber());
            assertEquals("user@gmail.com", info.getEmail());
            assertEquals(true, info.getIsEmployee());
            assertEquals(DOMESTIC_LABOR, info.getUserClass());
        } catch (Exception e) {
            Assert.fail("Không thể lấy thông tin người dùng");
        }
    }

    // Tính tiền
    @Test
    @Rollback
    @Transactional
    // Tiền điều kiện: Sau khi user đăng nhập
    // Input: null
    // Expect output: List<InsuranceFeeTimeRangeDTO> dto (InsuranceFeeTimeRangeDTO = startTime, endTime, fee)
    //      và tổng tiền phải đóng getTotalFee()
    // Cách tích tiền:
    public void calculateInsuranceValid() {
        try {
            List<DeclareIncomeDTO> incomeDTOS = new ArrayList<>();
            incomeDTOS.add(new DeclareIncomeDTO(sdf.parse("01/02/2010"), sdf.parse("31/03/2010"), 20000000));
            incomeDTOS.add(new DeclareIncomeDTO(sdf.parse("01/03/2021"), sdf.parse("30/04/2021"), 30000000));
            DeclareIncomeRequestDTO dto = new DeclareIncomeRequestDTO(incomeDTOS);
            applicationUserService.declareIncome(dto);
            CalculateInsuranceFeeResponseDTO res = applicationUserService.calculateInsurance();
            assertNotNull(res);
            assertEquals(Math.round(res.getTotalFee()), 7200000);
        }catch (ParseException ex){
            Assert.fail("Sai định dạng ngày");
        }

    }

//    // Get my profile
//    @Test
//    @Rollback
//    @Transactional
//    // Input:
//    // Expect output:
//    public void getMyProfile() {
//        UserInfoDTO dto = applicationUserService.getMyProfile();
//        assertNotNull(dto);
//        assertEquals(dto.getUsername(), "admin");
//        assertEquals(dto.getPhoneNumber(), "0123456789");
//        assertEquals(dto.getEmail(), "user@gmail.com");
//    }

    public void setAuthenByToken(String token) {
        Jws<Claims> claimsJws = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token);
        Claims body = claimsJws.getBody();
        String username = body.getSubject();
        List<Map<String, String>> authorities = (List<Map<String, String>>) body.get("authorities");
        Set<SimpleGrantedAuthority> simpleGrantedAuthorities = authorities.stream()
                .map(m -> new SimpleGrantedAuthority(m.get("authority")))
                .collect(Collectors.toSet());
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                username,
                null,
                simpleGrantedAuthorities
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
