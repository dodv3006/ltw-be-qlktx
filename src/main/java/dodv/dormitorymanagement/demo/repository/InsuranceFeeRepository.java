package dodv.dormitorymanagement.demo.repository;

import dodv.dormitorymanagement.demo.entity.InsuranceFee;
import dodv.dormitorymanagement.demo.enumuration.EUserClass;
import org.springframework.boot.context.annotation.UserConfigurations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface InsuranceFeeRepository extends JpaRepository<InsuranceFee, Integer> {
    @Query("SELECT isf FROM InsuranceFee isf WHERE isf.userClass = ?1 and isf.isEmployee = ?2")
    List<InsuranceFee> getInsuranceFeeByTimeRage(EUserClass userClass, Boolean isEmployee);
    List<InsuranceFee> findAll();
}
