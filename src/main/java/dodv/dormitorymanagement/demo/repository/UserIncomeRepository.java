package dodv.dormitorymanagement.demo.repository;

import dodv.dormitorymanagement.demo.dto.response.DateRangeDTO;
import dodv.dormitorymanagement.demo.entity.User;
import dodv.dormitorymanagement.demo.entity.UserIncome;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserIncomeRepository extends JpaRepository<UserIncome, Integer> {
    @Query("Select new dodv.dormitorymanagement.demo.dto.response.DateRangeDTO( MIN(ui.startTime) , MAX(ui.endTime)) from UserIncome ui where ui.user = ?1")
    DateRangeDTO findDateRangeByUser(User user);
    List<UserIncome> getUserIncomeByUser(User u);
}
