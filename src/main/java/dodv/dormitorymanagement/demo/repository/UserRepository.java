package dodv.dormitorymanagement.demo.repository;

import dodv.dormitorymanagement.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByInsuranceCodeIs(String code);
    User findByUsername(String username);

}
