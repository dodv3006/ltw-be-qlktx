package dodv.dormitorymanagement.demo.auth;

import dodv.dormitorymanagement.demo.dto.request.*;
import dodv.dormitorymanagement.demo.dto.response.*;
import dodv.dormitorymanagement.demo.entity.InsuranceFee;
import dodv.dormitorymanagement.demo.entity.User;
import dodv.dormitorymanagement.demo.entity.UserIncome;
import dodv.dormitorymanagement.demo.enumuration.EUserClass;
import dodv.dormitorymanagement.demo.repository.InsuranceFeeRepository;
import dodv.dormitorymanagement.demo.repository.UserIncomeRepository;
import dodv.dormitorymanagement.demo.repository.UserRepository;
import dodv.dormitorymanagement.demo.security.ApplicationUserRole;
import dodv.dormitorymanagement.demo.security.jwt.JwtConfig;
import io.jsonwebtoken.Jwts;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.server.ResponseStatusException;

import javax.crypto.SecretKey;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ApplicationUserService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserIncomeRepository userIncomeRepository;
    @Autowired
    private InsuranceFeeRepository insuranceFeeRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private JwtConfig jwtConfig;
    @Autowired
    private SecretKey secretKey;
    @Value("${application.start-date}")
    private String startDate;
    @Value("${application.end-date}")
    private String endDate;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new ApplicationUser(user);
    }

   public void register(@NotNull RegisterRequestDTO registerRequestDTO){
        try {
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String encodedPassword = passwordEncoder.encode(registerRequestDTO.getPassword());
            User addedUser = new User();
            addedUser.setPassword(encodedPassword);
            addedUser.setInsuranceCode(registerRequestDTO.getInsuranceCode());
            addedUser.setRole(ApplicationUserRole.USER);
            addedUser.setUsername(registerRequestDTO.getUsername());
            userRepository.save(addedUser);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Đăng ký thất bại");
        }
    }

    public final User getUser() {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null) {
                String userCd = auth.getName();
                return userRepository.findByUsername(userCd);
            }
            return new User();
        } catch (Exception ex) {
            return new User();
        }
    }

    public ResponseEntity<?> login(@RequestBody LoginRequestDTO input) {
        UserDetails user = loadUserByUsername(input.getUsername());
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if(user == null || !passwordEncoder.matches(input.getPassword(), user.getPassword())){
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(ErrorDTO.builder().error("Your username/password is not correct").build());
        }
        String token = Jwts.builder()
                .setSubject(input.getUsername())
                .claim("authorities", user.getAuthorities())
                .setIssuedAt(new Date())
                .setExpiration(java.sql.Date.valueOf(LocalDate.now().plusDays(jwtConfig.getTokenExpirationAfterDays())))
                .signWith(secretKey)
                .compact();
        return ResponseEntity.status(HttpStatus.OK).body(JWTResponseDTO.builder().token(token).build());
    }

    public void updateProfile(@NotNull DeclareInformationDTO input){
        try {
            User currentUser = getUser();
            currentUser.setFullName(input.getFullName());
            currentUser.setPhoneNumber(input.getPhoneNumber());
            currentUser.setEmail(input.getEmail() != null ? input.getEmail(): null);
            currentUser.setIsEmployee(input.getIsEmployee());
            currentUser.setUserClass(input.getUserClass());
            userRepository.save(currentUser);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED, "Cập nhật thông tin thất bại");
        }
    }

    @Transactional
    public void declareIncome(@NotNull DeclareIncomeRequestDTO input) {
        User currentUser = getUser();
        List<UserIncome> userIncomeList = userIncomeRepository.getUserIncomeByUser(currentUser);
        for(DeclareIncomeDTO x : input.getIncomes()){
            if(!checkIncomeIsValid(x,userIncomeList)){
                throw new ResponseStatusException(HttpStatus.EXPECTATION_FAILED,"Your income your declared is not valid");
            }
        }
        for(DeclareIncomeDTO x: input.getIncomes()){
            UserIncome ui = modelMapper.map(x, UserIncome.class);
            ui.setUser(currentUser);
            userIncomeRepository.save(ui);
        }

    }

    public List<FilterDRO> getUserClassFilter() {
        List<FilterDRO> res = new ArrayList<>();
        for(EUserClass userClass : EUserClass.values()){
            res.add(new FilterDRO(userClass.name(), userClass.getValue()));
        }
        return res;
    }

    public List<InsuranceFee> getInsuranceConfig() {
        List<InsuranceFee> list = new ArrayList<>();
        try {
            list = insuranceFeeRepository.findAll();
        } catch (Exception e) {

        }
        return list;
    }

    public CalculateInsuranceFeeResponseDTO calculateInsurance(){
        CalculateInsuranceFeeResponseDTO res = new CalculateInsuranceFeeResponseDTO();
        List<InsuranceFeeTimeRangeDTO> list = new ArrayList<>();
        User currentUser = this.getUser();
//        DateRangeDTO timeRange = userIncomeRepository.findDateRangeByUser(currentUser);
        List<InsuranceFee> insuranceFeeByUser = insuranceFeeRepository.getInsuranceFeeByTimeRage(currentUser.getUserClass(), currentUser.getIsEmployee());
        List<UserIncome> userIncomeList = userIncomeRepository.getUserIncomeByUser(currentUser);
        for(UserIncome x: userIncomeList){
           list.add(calInsuranceFeeTimeRange(x, insuranceFeeByUser)) ;
        }
        res.setStatistic(list);
        return res;
    }
    private InsuranceFeeTimeRangeDTO calInsuranceFeeTimeRange(UserIncome userIncome, List<InsuranceFee> insuranceFeeByUser){
        InsuranceFeeTimeRangeDTO res = new InsuranceFeeTimeRangeDTO();
        res.setStartTime(userIncome.getStartTime());
        res.setEndTime(userIncome.getEndTime());
        Float totalFee = 0f;
        for(InsuranceFee x: insuranceFeeByUser)
            totalFee += this.calculateFee(x,userIncome);
        res.setFee(totalFee);
        return res;
    }
    private Float calculateFee(InsuranceFee isf, UserIncome ui){
        if((isf.getEndTime() == null && isf.getStartTime() == null) || (ui.getStartTime().compareTo(isf.getStartTime()) >= 0 && ui.getEndTime().compareTo(isf.getEndTime()) <= 0))
            return ui.getIncome()*monthsBetweenDates(ui.getStartTime(), ui.getEndTime())*isf.getValue()/100;
        else if(ui.getEndTime().before(isf.getStartTime()) || ui.getStartTime().after(isf.getEndTime()))
            return 0f;
        else if(ui.getStartTime().compareTo(isf.getStartTime()) <= 0 && ui.getEndTime().compareTo(isf.getEndTime()) <= 0)
            return monthsBetweenDates(isf.getStartTime(), ui.getEndTime())*ui.getIncome()*isf.getValue()/100;
        else if(ui.getStartTime().compareTo(isf.getStartTime()) >= 0 && ui.getStartTime().before(isf.getEndTime()) && (ui.getEndTime().compareTo(isf.getEndTime()) >= 0))
            return monthsBetweenDates(ui.getStartTime(), isf.getEndTime())*ui.getIncome()*isf.getValue()/100;
        return 0f;
    }
    private long monthsBetweenDates(Date startDate, Date endDate){

        return ChronoUnit.MONTHS.between(
              LocalDate.parse(sdf.format(startDate)).withDayOfMonth(1),
                LocalDate.parse(sdf.format(endDate)).withDayOfMonth(1)) + 1;
    }

    public UserInfoDTO getMyProfile() {
        User currentUser = this.getUser();
        UserInfoDTO res = modelMapper.map(currentUser,UserInfoDTO.class);
        List<UserIncomeDTO> x = new ArrayList<>();
        for(UserIncome ui: currentUser.getIncomes())
        {
            UserIncomeDTO uid =  modelMapper.map(ui,UserIncomeDTO.class);
            x.add(uid);
        }
        res.setIncomes(x);
        return res;
    }

    private boolean checkIncomeIsValid(DeclareIncomeDTO input, List<UserIncome> declaredIncomes) {
        try {
            if(input.getStartTime().after(input.getEndTime())) return false;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if(input.getStartTime().before(sdf.parse(startDate)) || input.getEndTime().after(sdf.parse(endDate)))
                return false;
            for(UserIncome s: declaredIncomes)
            {
                if(!(input.getEndTime().before(s.getStartTime()) || input.getStartTime().after(s.getEndTime()))) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
