package dodv.dormitorymanagement.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dodv.dormitorymanagement.demo.enumuration.EUserClass;
import dodv.dormitorymanagement.demo.security.ApplicationUserRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.util.List;

@Table(name = "tbl_user")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private int id;
    @Column(name ="insurance_code", unique = true, nullable = false)
    private String insuranceCode;
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "username", length = 10, unique = true)
    private String username;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "email")
    @Email
    private String email;
    @Column(name = "is_employee")
    private Boolean isEmployee;
    @Column(name = "user_class")
    @Enumerated(EnumType.STRING)
    private EUserClass userClass;
    @Column(name = "encryptedPassword")
    @JsonIgnore
    private String password;
    @Column(name ="role")
    private ApplicationUserRole role;
    @OneToMany(mappedBy = "user")
    List<UserIncome> incomes;
//    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
//    @PrimaryKeyJoinColumn
//    private UserAddress userAddress;



}
