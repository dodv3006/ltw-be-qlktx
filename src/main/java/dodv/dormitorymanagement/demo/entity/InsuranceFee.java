package dodv.dormitorymanagement.demo.entity;

import dodv.dormitorymanagement.demo.enumuration.EUserClass;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Table(name = "tbl_insurance_fee")
@Entity
@Getter
@Setter
@NoArgsConstructor
public class InsuranceFee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private int id;
    @Column(name = "start_time")
    @Temporal(TemporalType.DATE)
    private Date startTime;
    @Temporal(TemporalType.DATE)
    @Column(name = "end_time")
    private Date endTime;
    @Column(name = "user_class")
    @Enumerated(EnumType.STRING)
    private EUserClass userClass;
    @Column(name = "is_employee")
    private Boolean isEmployee;
    @Column(name = "value")
    private Float value;

}
