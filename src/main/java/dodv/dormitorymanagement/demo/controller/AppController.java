package dodv.dormitorymanagement.demo.controller;

import dodv.dormitorymanagement.demo.auth.ApplicationUserService;
import dodv.dormitorymanagement.demo.dto.request.DeclareIncomeDTO;
import dodv.dormitorymanagement.demo.dto.request.DeclareIncomeRequestDTO;
import dodv.dormitorymanagement.demo.dto.request.DeclareInformationDTO;
import dodv.dormitorymanagement.demo.dto.response.CalculateInsuranceFeeResponseDTO;
import dodv.dormitorymanagement.demo.dto.response.FilterDRO;
import dodv.dormitorymanagement.demo.dto.response.UserInfoDTO;
import dodv.dormitorymanagement.demo.entity.InsuranceFee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AppController {
    @Autowired
    private ApplicationUserService applicationUserService;
    @PostMapping("/update-profile")
    public void updateProfile(@RequestBody DeclareInformationDTO input){
        applicationUserService.updateProfile(input);
    }
    @PostMapping("/declare-income")
    public void declareIncome(@RequestBody DeclareIncomeRequestDTO input){
        applicationUserService.declareIncome(input);
    }
    @GetMapping("/user-class-filter")
    public List<FilterDRO> getUserClassFilter(){
        return applicationUserService.getUserClassFilter();
    }
    @GetMapping("/calculate-insurance-fee")
    public CalculateInsuranceFeeResponseDTO calculateInsuranceFee(){
        return applicationUserService.calculateInsurance();
    }
    @GetMapping("/get-my-profile")
    public UserInfoDTO getMyProfile(){
        return applicationUserService.getMyProfile();
    }
    @GetMapping("/get-insurance-config")
    public List<InsuranceFee> getInsuranceConfig() {
        return applicationUserService.getInsuranceConfig();
    }
}
