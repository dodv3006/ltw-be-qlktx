package dodv.dormitorymanagement.demo.controller;

import dodv.dormitorymanagement.demo.auth.ApplicationUser;
import dodv.dormitorymanagement.demo.auth.ApplicationUserService;
import dodv.dormitorymanagement.demo.dto.request.LoginRequestDTO;
import dodv.dormitorymanagement.demo.dto.request.RegisterRequestDTO;
import dodv.dormitorymanagement.demo.dto.response.ErrorDTO;
import dodv.dormitorymanagement.demo.dto.response.JWTResponseDTO;
import dodv.dormitorymanagement.demo.security.jwt.JwtConfig;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.crypto.SecretKey;
import java.time.LocalDate;
import java.util.Date;


@RestController

public class AuthController {
    @Autowired
    ApplicationUserService applicationUserService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequestDTO input){
      return applicationUserService.login(input);
    }
    @PostMapping("/register")
    public void register(@RequestBody RegisterRequestDTO input){
        applicationUserService.register(input);
    }


}
