package dodv.dormitorymanagement.demo.enumuration;

public enum EUserClass {
    DOMESTIC_LABOR("Người lao động Việt Nam"),
    FOREIGN_LABOR("Người lao động nước ngoài"),
    OFFICER("Cán bộ, công chức, viên chức"),
    ;
    private final String value;
    EUserClass(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
