package dodv.dormitorymanagement.demo.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalculateInsuranceFeeResponseDTO {
    private List<InsuranceFeeTimeRangeDTO> statistic;
    public Float getTotalFee(){
        Float totalFee = 0f;
        for(InsuranceFeeTimeRangeDTO x : statistic)
            totalFee += x.getFee();
        return totalFee;
    }
}
