package dodv.dormitorymanagement.demo.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DateRangeDTO {
    private Date oldestDate;
    private Date newestDate;
}
