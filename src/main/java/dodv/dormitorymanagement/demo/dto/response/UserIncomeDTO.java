package dodv.dormitorymanagement.demo.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserIncomeDTO {
    private Date startTime;
    private Date endTime;
    private Integer income;
}
