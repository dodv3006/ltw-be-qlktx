package dodv.dormitorymanagement.demo.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InsuranceFeeTimeRangeDTO {
    private Date startTime;
    private Date endTime;
    private Float fee;
}
