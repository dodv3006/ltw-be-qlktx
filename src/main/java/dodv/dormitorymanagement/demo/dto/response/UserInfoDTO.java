package dodv.dormitorymanagement.demo.dto.response;

import dodv.dormitorymanagement.demo.entity.UserIncome;
import dodv.dormitorymanagement.demo.enumuration.EUserClass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInfoDTO {
    private String insuranceCode;
    private String fullName;
    private String username;
    private String phoneNumber;
    private String email;
    private Boolean isEmployee;
    private EUserClass userClass;
    List<UserIncomeDTO> incomes;
}
