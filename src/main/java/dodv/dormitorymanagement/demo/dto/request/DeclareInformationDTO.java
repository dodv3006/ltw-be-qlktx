package dodv.dormitorymanagement.demo.dto.request;

import dodv.dormitorymanagement.demo.enumuration.EUserClass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeclareInformationDTO {
    private String fullName;
   private String phoneNumber;
    private String email;
    private Boolean isEmployee;
    private EUserClass userClass;
}
